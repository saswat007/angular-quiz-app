import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isQuizRoute: boolean = false;
  isQNARoute: boolean = false;
  isAddQuestionRoute: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.updateButtonState();
    });
  }

  goToAdd(){
    this.router.navigate(['/addQuestion']);
  }

  goToQNA(){
    this.router.navigate(['/QNA']);
  }

  goToQuiz(){
    this.router.navigate(['/quiz']);
  }

  updateButtonState(): void {
    const currentRoute = this.router.url;
    this.isQuizRoute = currentRoute.includes('/quiz');
    this.isAddQuestionRoute = currentRoute.includes('/addQuestion');
    this.isQNARoute = currentRoute.includes('/QNA');
  }

}
