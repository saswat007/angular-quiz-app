import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroupDirective, Validators } from '@angular/forms';
import { QnaService } from 'src/app/shared/services/qna.service';


@Component({
  selector: 'app-quiz-dashboard',
  templateUrl: './quiz-dashboard.component.html',
  styleUrls: ['./quiz-dashboard.component.css']
})
export class QuizDashboardComponent implements OnInit {
  firstFormGroup;
  secondFormGroup;
  isLinear;
  categoryOptions;
  qnaList;
  selectedCategories;
  filteredQuestions;
  noOfquestionsOptions = [];
  private timer: any;
  timeInSeconds: number = 60; // Initial time in seconds
  displayTime: string = this.getSecondsAsDigitalClock(this.timeInSeconds);
  remainingCounterTime;
  threeSecondCountdown;
  isCounterVisible: boolean = false;
  isQuizFormVisible: boolean = true;
  isQuizQuestionVisible: boolean = false;
  finalQuizQuestionArray;
  currentQuestion;
  questionCounter = 0;
  @ViewChild('formDirective') formGroupDirective: FormGroupDirective;
  @ViewChild('formDirectiveDropDown') formGroupDirectiveDropDown: FormGroupDirective;


  constructor(private _formBuilder: FormBuilder, private qnaService: QnaService) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required],
    });
    this.isLinear = true;
  }

  ngOnInit(): void {
    this.getCategories()
  }

  getCategories() {
    this.qnaService.getCategories().subscribe((response) => {
      this.categoryOptions = response.filter(item => item.name != 'All');
    })
  }

  getQNA() {
    this.qnaService.getQNA().subscribe((response) => {
      this.filteredQuestions = response.filter(item => this.selectedCategories.includes(item.category))
      this.setQuestionsLimit();
    })
  }


  filterQuestions() {
    this.selectedCategories = this.firstFormGroup.value;
    this.selectedCategories = this.selectedCategories.firstCtrl;
    if (this.firstFormGroup.valid) {
      this.getQNA();
    }
  }

  setQuestionsLimit() {
    this.noOfquestionsOptions = [];
    let questionsCount = this.filteredQuestions.length;
    for (let i = this.selectedCategories.length; i <= questionsCount; i++) {
      this.noOfquestionsOptions.push(i);
    }
  }

  setFinalQuestions() {
    let finalQuestionsArray = [];
    let selectedValue = this.secondFormGroup.value;
    let questionsLimit = selectedValue.secondCtrl;
    console.log(questionsLimit);
    const shuffledQuestions = this.shuffleArray(this.filteredQuestions);
    const uniqueCategoryQuestions = this.getOneElementFromEachCategory(shuffledQuestions)
    const remainingQuestions = shuffledQuestions.filter(question =>
      !uniqueCategoryQuestions.some(uniqueQuestion => uniqueQuestion.id === question.id)
    );
    const combinedQuestions = [...uniqueCategoryQuestions, ...remainingQuestions]
    console.log(combinedQuestions);
    for (let i = 0; i < questionsLimit; i++) {
      finalQuestionsArray[i] = combinedQuestions[i]
    }
    finalQuestionsArray.forEach((element,index)=>{
      element.index = index + 1
    })
    this.finalQuizQuestionArray = finalQuestionsArray;
    console.log(this.finalQuizQuestionArray);
  }

  shuffleArray(array: any[]): any[] {
    return array.slice().sort(() => Math.random() - 0.5);
  }

  getOneElementFromEachCategory(shuffledQuestions): any[] {
    const categoryMap = new Map<string, any>();
    for (const item of shuffledQuestions) {
      if (!categoryMap.has(item.category)) {
        categoryMap.set(item.category, item);
      }
    }
    const resultArray = Array.from(categoryMap.values());
    return resultArray;
  }

  startQuiz(){
    this.isQuizQuestionVisible = true;
    this.currentQuestion = this.finalQuizQuestionArray[this.questionCounter];
  }

  nextQuestion(){
    if(!(this.currentQuestion?.index == this.finalQuizQuestionArray.length)){
      console.log(this.currentQuestion?.index);
      console.log(this.finalQuizQuestionArray.length);
      this.questionCounter++;
      this.startQuiz();
    }
  }

  resetAll(){
    this.resetForms();
    this.isQuizQuestionVisible = false;
    this.isQuizFormVisible = true;
    this.questionCounter = 0;
    this.finalQuizQuestionArray = null;
  }

  resetForms() {
    setTimeout(() =>
      this.formGroupDirective.resetForm(), 0);
    setTimeout(() =>
      this.formGroupDirectiveDropDown.resetForm(), 0);
  }

  startTimer() {
    this.timer = setInterval(() => {
      if (this.timeInSeconds > 0) {
        this.timeInSeconds--;
        this.displayTime = this.getSecondsAsDigitalClock(this.timeInSeconds);
      } else {
        this.stopTimer();
      }
    }, 1000);
  }

  threeSecondTimer() {
    this.isQuizFormVisible = false;
    this.isCounterVisible = true;
    let timeRemaining = 4.5;
    this.threeSecondCountdown = setInterval(() => {
      if (timeRemaining > 0) {
        timeRemaining--;
        this.remainingCounterTime = timeRemaining;
      } else {
        this.questionCounter = 0;
        this.isCounterVisible = false;
        this.stopThreeSecondTimer();
        //this.startTimer();
        this.startQuiz();
      }
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.timer);
  }

  stopThreeSecondTimer() {
    clearInterval(this.threeSecondCountdown);
  }

  getSecondsAsDigitalClock(seconds: number): string {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const remainingSeconds = seconds % 60;

    const hoursStr = (hours < 10) ? '0' + hours : hours;
    const minutesStr = (minutes < 10) ? '0' + minutes : minutes;
    const secondsStr = (remainingSeconds < 10) ? '0' + remainingSeconds : remainingSeconds;

    return `${hoursStr}:${minutesStr}:${secondsStr}`;
  }

}


