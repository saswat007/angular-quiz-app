import { TemplateDownloadService } from './../../../shared/services/template-download.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { QnaService } from 'src/app/shared/services/qna.service';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Workbook } from 'exceljs';


@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {
  questionForm: FormGroup;
  categoryForm: FormGroup;
  categoryOptions: Object[];
  file;
  @ViewChild('formDirective') formGroupDirective: FormGroupDirective;

  constructor(private qnaService: QnaService, private fb: FormBuilder, private _snackBar: MatSnackBar, private templateDownloadService: TemplateDownloadService) {
    this.questionForm = this.fb.group({
      category: ['', Validators.required],
      content: ['', Validators.required],
      answer: ['', Validators.required],
      link: ['']
    })

    this.categoryForm = this.fb.group({
      name: ['', [this.noWhitespaceValidator]]
    })
  }

  ngOnInit(): void {
    this.getCategories();
  }

  addQuestion() {
    if (this.questionForm.valid) {
      const newQuestion = this.questionForm.value;
      console.log(newQuestion);
      this.qnaService.addQuestion(newQuestion).subscribe((response) => {
        console.log('question added successfully', response);
        this.resetQuestionForm();
        this.openSnackBar('Question added successfully', 'Ok');
      }, error => {
        console.log('error in adding question', error);
      })
    } else {
      // invalid form
    }
  }

  addCategory() {
    const newCategory = this.categoryForm.value;
    let existingCategory = '';
    if (newCategory.name != '') {
      this.categoryOptions.forEach((ele: any) => {
        if (ele.name.toLowerCase() === newCategory.name.toLowerCase().trim()) {
          existingCategory = newCategory.name
        }
      })
    }

    if (newCategory.name != '' && existingCategory == '') {
      this.qnaService.addCategory(newCategory).subscribe((response) => {
        this.openSnackBar('Category added successfully', 'Ok');
        console.log('category added successfully');
        this.categoryForm.reset();
        this.getCategories();
      }, error => {
        console.log('error in adding category');
      })
    } else {
      this.openSnackBar('Category already exists', 'Ok');
      this.categoryForm.reset();
    }
  }

  getCategories() {
    this.qnaService.getCategories().subscribe((response) => {
      this.categoryOptions = response.filter(item => item.name != 'All');
    })
  }

  resetQuestionForm() {
    //this.questionForm.reset();
    setTimeout(() =>
      this.formGroupDirective.resetForm(), 0)
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 3000
    });
  }

  noWhitespaceValidator(control) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  downloadTemplate(): void {
    this.templateDownloadService.downloadTemplate();
  }

async readExcel() {
  try {
      let wb = new Workbook();
      await wb.xlsx.load(this.file);
      let ws = wb.getWorksheet('Sheet1');
      let data = [];
      for (let i = 2; i <= ws.rowCount; i++) {
          let category = ws.getCell(i, 1).value;
          let content = ws.getCell(i, 2).value;
          let answer = ws.getCell(i, 3).value;
          let link = ws.getCell(i, 4).value;
          data.push({ category, content, answer, link });
      }
      console.log(data);
      //return data;
  } catch (err) {
      console.log(err.message);
  }
}

fileSelect(event){
this.file = event.target.files[0];
this.readExcel();
}
}
