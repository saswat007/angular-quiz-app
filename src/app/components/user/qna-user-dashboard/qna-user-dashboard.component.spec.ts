import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QnaUserDashboardComponent } from './qna-user-dashboard.component';

describe('QnaUserDashboardComponent', () => {
  let component: QnaUserDashboardComponent;
  let fixture: ComponentFixture<QnaUserDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QnaUserDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QnaUserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
