import { QnaService } from './../../../shared/services/qna.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qna-user-dashboard',
  templateUrl: './qna-user-dashboard.component.html',
  styleUrls: ['./qna-user-dashboard.component.css']
})
export class QnaUserDashboardComponent implements OnInit {
  qnaList:any;
  panelOpenState = false;
  selectedCategoryValue: string = 'All';
  options: string[] = ['All','JavaScript', 'Angular', 'HTML/CSS', 'Bootstrap', 'Angular Material'];
  filteredQnaList: any[];
  categoryOptions: Object[];

  constructor(private qnaService: QnaService) { }

  ngOnInit(): void {
    this.qnaService.getCategories().subscribe((response)=>{
      this.categoryOptions = response;
    })
    this.qnaService.getQNA().subscribe((response)=>{
      this.qnaList = response;
      this.filteredQnaList = this.qnaList;
    })
  }

  onCategoryChange(){
    this.filteredQnaList = this.selectedCategoryValue === 'All' ? this.qnaList : this.qnaList.filter(item=>
      item.category === this.selectedCategoryValue
    )
  }

}
