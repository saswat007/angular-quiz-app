import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { QnaUserDashboardComponent } from './components/user/qna-user-dashboard/qna-user-dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from './modules/my-material/my-material.module';
import { FormsModule } from '@angular/forms';
import { NavBarComponent } from './components/user/nav-bar/nav-bar.component';
import { AddQuestionComponent } from './components/user/add-question/add-question.component';
import { QuizDashboardComponent } from './components/user/quiz-dashboard/quiz-dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    QnaUserDashboardComponent,
    NavBarComponent,
    AddQuestionComponent,
    QuizDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
