import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QnaUserDashboardComponent } from './components/user/qna-user-dashboard/qna-user-dashboard.component';
import { AddQuestionComponent } from './components/user/add-question/add-question.component';
import { QuizDashboardComponent } from './components/user/quiz-dashboard/quiz-dashboard.component';


const routes: Routes = [
  { path: '',redirectTo: '/QNA', pathMatch: 'full'},
  { path: 'QNA', component: QnaUserDashboardComponent},
  { path: 'addQuestion', component: AddQuestionComponent},
  { path: 'quiz', component: QuizDashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
