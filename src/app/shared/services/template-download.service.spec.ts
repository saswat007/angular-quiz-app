import { TestBed } from '@angular/core/testing';

import { TemplateDownloadService } from './template-download.service';

describe('TemplateDownloadService', () => {
  let service: TemplateDownloadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TemplateDownloadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
