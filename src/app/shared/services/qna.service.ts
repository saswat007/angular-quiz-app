import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QnaService {
  private apiURL = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getQNA(): Observable <any[]> {
    return this.http.get<any[]>(this.apiURL+'/questions');
  }

  addQuestion(question: any): Observable<any> {
    return this.http.post(this.apiURL+'/questions', question);
  }

  addCategory(category: any) : Observable<any> {
    return this.http.post(this.apiURL+'/categories',category);
  }

  getCategories() : Observable <any[]> {
    return this.http.get<any[]>(this.apiURL+'/categories');
  }

}
